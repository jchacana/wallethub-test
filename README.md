## README

# Database cretion
In order to create a MySQL database, a docker-compose file has been provided. 
It can be run as `docker-compose up` to spin up database and `docker-compose down` to destroy container
A database called `test` will be created
Once created, when the application is started it will create tables automatically through Liquibase

# Compiling
This project can be compiled via maven, either with your own local maven installation or
maven wrapper included. Commands are:
* Local: `mvn clean package`
* Wrapper (UNIX-like SO): `./mvnw clean package`
* Wrapper (Windows SO): `mvnw.cmd clean package`

# File Location
Before compiling, the log location needs to be provided as an absolute path. For that reason, a property is set under `application.properties` file,
property `com.ef.log.filename`. If it can't find file, program execution will fail

# Execution
To execute this program (originally started as a Spring Boot App), it was necessary to add a custom JarClassLoader. 
With this, execution can be like `java -cp "parser.jar" com.ef.Parser --startDate=2017-01-01.13:00:00 --duration=daily --threshold=250`

# Query Examples
You can find query examples at `resources/db/queries` with schema creation and queries 

