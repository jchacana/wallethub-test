
-- This Query gets all the IP and Request Count given an interval, starting from the provided date and if request count
-- is greater than threshold

-- Replace '2017-01-01T00:00:00' with desired date
-- Replace 500 with desired threshold. At application level it's validated
-- Replace DAY with HOUR to change duration from "daily" to "hourly"
SELECT ip, count(1) as requestCount FROM access_log WHERE
  request_date >= DATE('2017-01-01T00:00:00') AND
  request_date <= DATE_ADD(DATE('2017-01-01T00:00:00'), INTERVAL 1 DAY )
GROUP BY ip HAVING requestCount > 500;


-- This Query gests the request count from any given ip
-- Replace ip value with desired ip to look up
SELECT ip, count(1) as requestCount from access_log
  WHERE ip = '192.168.169.178';