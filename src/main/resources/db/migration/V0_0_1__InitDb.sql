DROP TABLE IF EXISTS test.access_log;
DROP TABLE IF EXISTS test.blocked_ip;

CREATE TABLE test.access_log (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    request_date DATETIME NOT NULL,
    ip VARCHAR(15) NOT NULL,
    method VARCHAR(20) NOT NULL,
    status_code INT(3) NOT NULL,
    user_agent VARCHAR(300) NOT NULL
);

CREATE TABLE test.blocked_ip (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    ip VARCHAR(15) NOT NULL,
    request_count INT(20) NOT NULL,
    reason VARCHAR(300) NOT NULL
);