package com.ef.utils;

import com.ef.executor.AllowedParams;
import com.ef.executor.Duration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

/**
 * Created by jchacana on 9/2/18.
 */
@Component
public class ArgumentValidator {

    private Map<String, Object> argsMap = new HashMap<>();
    private final static Set<String> validKeys = new HashSet<>(Arrays.asList(AllowedParams.START_DATE.param,
            AllowedParams.DURATION.param, AllowedParams.THRESHOLD.param));
    private final static Set<String> validDurations = new HashSet<>(Arrays.asList(Duration.DAILY.key, Duration.HOUR.key));

    @Value("${com.ef.startdate.format}")
    private String dateFormat;

    @Value("${com.ef.duration.hourly.limit}")
    private Integer hourlyLimit;

    @Value("${com.ef.duration.daily.limit}")
    private Integer dailyLimit;

    public boolean isValidArgs(String[] arguments) throws ParseException {
        if(arguments.length == 0) return false;
        if(arguments.length != 3) return false;

        for(String arg: arguments) {
            parseArg(arg);
        }
        return true;
    }

    private void parseArg(String arg) throws ParseException {
        if(!StringUtils.startsWithIgnoreCase(arg, "--")) throw new ParseException("Error parsing argument " + arg, -1);
        String parsedArg = arg.substring(2);
        String[] values = parsedArg.split("=");
        if(values.length != 2) throw new ParseException("Error parsing argument" + arg, -1);
        if(!isValidKey(values[0])) throw new ParseException("Invalid argument " + values[0], -1);
        if(!parseKeyValue(values[0], values[1])) throw new ParseException("Invalid value " + values[1] + " for argument " + values[0], -1);

    }

    private boolean parseKeyValue(String key, String value) {
        try{
            switch (key) {
                case "startDate":
                    argsMap.put(key, parseDate(value));
                    return true;
                case "duration":
                    if(validDurations.contains(value)){
                        argsMap.put(key, value);
                        return true;
                    }
                    break;
                case "threshold":
                    argsMap.put(key, parsePossibleInteger(value, key));
                    return true;
            }
        } catch (ParseException e) {
            return false;
        }
        return false;
    }

    private Integer parsePossibleInteger(String value, String key) {
        Integer number = 0;
        try {
            number = Integer.parseInt(value);
        } catch (NumberFormatException ex) {
            number = 0;
        }
        if(number > hourlyLimit && Duration.HOUR.key.equals(key) ) number = hourlyLimit;
        if(number > dailyLimit && Duration.DAILY.key.equals(key)) number = dailyLimit;
        if(number < 0 ) number = 0;
        return number;
    }

    private LocalDateTime parseDate(String value) throws ParseException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
        try{
            return LocalDateTime.parse(value, formatter);
        } catch (DateTimeParseException ex) {
            throw new ParseException("Error parsing date " + value, -1);
        }
    }

    private boolean isValidKey(String value) {
        return validKeys.contains(value);
    }

    public Map<String, Object> getArgsMap() {
        return argsMap;
    }

    public void setArgsMap(Map<String, Object> argsMap) {
        this.argsMap = argsMap;
    }
}
