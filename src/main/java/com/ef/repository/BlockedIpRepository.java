package com.ef.repository;

import com.ef.executor.BlockedIp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by jchacana on 9/4/18.
 */
@Repository
public interface BlockedIpRepository extends JpaRepository<BlockedIp, Long> {


}
