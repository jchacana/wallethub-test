package com.ef.repository;

import com.ef.executor.AccessLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by jchacana on 9/3/18.
 */
@Repository
public interface AccessLogRepository extends JpaRepository<AccessLog, Long> {

    @Query(nativeQuery = true,
            value = "SELECT ip, count(1) as \"requestCount\" FROM access_log WHERE\n" +
                    "  request_date >= DATE(:startDate) AND\n" +
                    "  request_date <= DATE_ADD(DATE(:startDate), INTERVAL 1 DAY )\n" +
                    "GROUP BY ip HAVING count(1) > :threshold")
    List<MAcessLog> findBlockedIpDaily(
            @Param("startDate") LocalDateTime startDate,
            @Param("threshold") Long threshold);

    @Query(nativeQuery = true,
            value = "SELECT ip, count(1) as \"requestCount\" FROM access_log WHERE\n" +
                    "  request_date >= DATE(:startDate) AND\n" +
                    "  request_date <= DATE_ADD(DATE(:startDate), INTERVAL 1 HOUR )\n" +
                    "GROUP BY ip HAVING count(1) > :threshold")
    List<MAcessLog> findBlockedIpHourly(
            @Param("startDate") LocalDateTime startDate,
            @Param("threshold") Long threshold);

    public static interface MAcessLog {
        String getIp();
        Long getRequestCount();
    }
}
