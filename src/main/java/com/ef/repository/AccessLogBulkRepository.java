package com.ef.repository;

import com.ef.executor.AccessLog;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by jchacana on 9/4/18.
 */
@Repository
public class AccessLogBulkRepository extends SimpleJpaRepository<AccessLog, Long> {


    private EntityManager entityManager;

    public AccessLogBulkRepository(EntityManager entityManager) {
        super(AccessLog.class, entityManager);
        this.entityManager = entityManager;
    }

    @Transactional
    public List<AccessLog> save(List<AccessLog> logs) {
        logs.forEach(log -> entityManager.persist(log));
        return logs;
    }
}
