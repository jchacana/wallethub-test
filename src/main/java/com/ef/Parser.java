package com.ef;

import com.jdotsoft.jarloader.JarClassLoader;

/**
 * Created by jchacana on 9/4/18.
 */
public class Parser {

    public static void main(String[] args) {
        JarClassLoader jcl = new JarClassLoader();
        try {
            jcl.invokeMain("com.ef.ParserExecutor", args);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}
