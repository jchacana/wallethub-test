package com.ef;

import java.lang.*;
import com.ef.executor.Executor;
import com.ef.utils.ArgumentValidator;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.Map;

@SpringBootApplication
@EntityScan
@EnableJpaRepositories
public class ParserExecutor implements CommandLineRunner{

	private ArgumentValidator validator;
	private Executor executor;

	public ParserExecutor(ArgumentValidator validator, Executor executor) {
		this.validator = validator;
		this.executor = executor;
	}

	public static void main(String[] args) {
		//disabled banner, don't want to see the spring logo
		SpringApplication app = new SpringApplication(ParserExecutor.class);
		app.setBannerMode(Banner.Mode.OFF);
		app.run(args);
	}

	@Override
	public void run(String... strings) throws Exception {
		if(validator.isValidArgs(strings)) {
			Map<String, Object> argsMap = validator.getArgsMap();
			executor.parseLog(argsMap);
		}
	}
}
