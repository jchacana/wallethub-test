package com.ef.executor;

/**
 * Created by jchacana on 9/4/18.
 */
public enum AllowedParams {

    START_DATE("startDate"),
    DURATION("duration"),
    THRESHOLD("threshold");

    public final String param;

    AllowedParams(String param) {
        this.param = param;
    }
}
