package com.ef.executor;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by jchacana on 9/3/18.
 */
@Entity
@Table(name = "blocked_ip")
public class BlockedIp {

    @Id
    @GeneratedValue(generator = "blocked_ip_generator")
    @GenericGenerator(name = "blocked_ip_generator", strategy = "increment")
    private Long id;

    @Column(name = "ip")
    private String ip;

    @Column(name = "request_count")
    private Long requestCount;

    @Column(name = "reason")
    private String reason;

    public BlockedIp() {
    }

    public BlockedIp(String ip, Long requestCount) {
        this.ip = ip;
        this.requestCount = requestCount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Long getRequestCount() {
        return requestCount;
    }

    public void setRequestCount(Long requestCount) {
        this.requestCount = requestCount;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "BlockedIp{" +
                "ip='" + ip + '\'' +
                ", requestCount=" + requestCount +
                ", reason='" + reason + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BlockedIp blockedIp = (BlockedIp) o;

        if (id != null ? !id.equals(blockedIp.id) : blockedIp.id != null) return false;
        if (ip != null ? !ip.equals(blockedIp.ip) : blockedIp.ip != null) return false;
        if (requestCount != null ? !requestCount.equals(blockedIp.requestCount) : blockedIp.requestCount != null)
            return false;
        return reason != null ? reason.equals(blockedIp.reason) : blockedIp.reason == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (ip != null ? ip.hashCode() : 0);
        result = 31 * result + (requestCount != null ? requestCount.hashCode() : 0);
        result = 31 * result + (reason != null ? reason.hashCode() : 0);
        return result;
    }
}
