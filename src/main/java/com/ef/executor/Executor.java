package com.ef.executor;

import com.ef.repository.AccessLogBulkRepository;
import com.ef.repository.AccessLogRepository;
import com.ef.repository.BlockedIpRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by jchacana on 9/2/18.
 */
@Component
public class Executor {

    @Autowired
    private AccessLogBulkRepository accessLogBulkRepository;

    @Autowired
    private AccessLogRepository accessLogRepository;

    @Autowired
    private BlockedIpRepository blockedIpRepository;

    @Value("${com.ef.log.filename}")
    private String fileName;

    @Value("${com.ef.log.dateformat}")
    private String dateFormat;

    public void parseLog(Map<String, Object> argsMap) {
        try (Stream<String> stream = Files.lines(Paths.get(fileName))){
            List<AccessLog> list = stream.map(line -> {
                String[] elems = line.split("\\|");
                return getAccessLog(elems);
            })
            .collect(Collectors.toList());
            accessLogBulkRepository.save(list);
            List<BlockedIp> blockedIp = findBlockedIp(argsMap);
            blockedIp.forEach(ip -> System.out.println(ip.getIp()));
            blockedIpRepository.saveAll(blockedIp);
        } catch (IOException e) {
            System.err.println("File couldn't be located. Please provide a file location " +
                    "at com.ef.log.filename on application.properties");
        }


    }

    private List<BlockedIp> findBlockedIp(Map<String, Object> argsMap) {
        LocalDateTime startDate = (LocalDateTime) argsMap.get(AllowedParams.START_DATE.param);
        String duration = getDuration(String.valueOf(argsMap.get(AllowedParams.DURATION.param)));

        List<BlockedIp> blockedIpList;
        Long threshold = getThreshold(argsMap);
        if(Duration.DAILY.interval.equals(duration)){
            blockedIpList = accessLogRepository.findBlockedIpDaily(
                    startDate,
                    threshold).stream().map(log ->{
                BlockedIp blockedIp = new BlockedIp(log.getIp(), log.getRequestCount());
                blockedIp.setReason("More than " + threshold + " requests in 1 " + duration);
                return blockedIp;
            }).collect(Collectors.toList());
        } else {
            blockedIpList = accessLogRepository.findBlockedIpHourly(
                    startDate,
                    threshold).stream().map(log -> {
                BlockedIp blockedIp = new BlockedIp(log.getIp(), log.getRequestCount());
                blockedIp.setReason("More than " + threshold + " requests in 1" + duration);
                return blockedIp;
            }).collect(Collectors.toList());
        }

        return blockedIpList;
    }

    private String getDuration(String duration) {
        return Duration.fromKey(duration).interval;
    }

    private AccessLog getAccessLog(String[] elems) {
        AccessLog log = new AccessLog();
        try {
            log.setRequestDate(parseDate(elems[0]));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        log.setIp(elems[1]);
        log.setMethod(elems[2]);
        log.setStatusCode(Integer.parseInt(elems[3]));
        log.setUserAgent(elems[4]);
        return log;
    }

    private long getThreshold(Map<String, Object> argsMap) {
        return ((Integer) argsMap.get("threshold")).longValue();
    }

    private LocalDateTime parseDate(String value) throws ParseException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
        try{
            return LocalDateTime.parse(value, formatter);
        } catch (DateTimeParseException ex) {
            throw new ParseException("Error parsing date " + value, -1);
        }
    }
}
