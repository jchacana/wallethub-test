package com.ef.executor;

/**
 * Created by jchacana on 9/4/18.
 */
public enum Duration {

    HOUR("hourly", "HOUR"),
    DAILY("daily", "DAY");

    public final String key;
    public final String interval;

    Duration(String key, String interval) {
        this.key = key;
        this.interval = interval;
    }

    static Duration fromKey(String key) {
        for(Duration duration: Duration.values()) {
            if( duration.key.equals(key)) return duration;
        }
        return null;
    }

}
